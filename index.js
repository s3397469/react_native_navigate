// Starting point of the App.
// Nothing changes here

// Read the comments I have added on each page
// Aeron.

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);


