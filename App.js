// This is the Landing page
// This is where you defin the nevigation options.

import React, { Component } from 'react';
import {
  Platform,  StyleSheet,
  Text,
  View
} from 'react-native';

// Importing the nevigation lib
import { StackNavigator } from 'react-navigation';

// The other screens to be worked in the nevigation
import SecondScreen from './screens/SecondScreen';
import Home from './screens/Home';


// This is the initialization of the StackNavigator object with all the links
// which will stay in the Application state.
const AppNavigator = StackNavigator({
  HomeScreen: { screen: Home },
  SecondScreen: { screen: SecondScreen }  
});

// This will be the default class works as a dummy to load the StackNavigator object.
export default class App extends Component {
  render() { 
    return (
        // This will load the First screen (Home) as the default page.
      <AppNavigator />
    );
  }
}