// Second Screen

import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';



export class SecondScreen extends Component {
  render() {
      
    // Obtain the values from the previous screen.
    const { navigation } = this.props;
    const customValue = navigation.getParam('customValue', 'None');
      
     
    return (
      <View>
        
        <Text style={styles.titleText} >
          Second Page{'\n'}{'\n'}
        
        Display the value from previous screen: {'\n'}{'\n'} {customValue}{'\n'}
        </Text>
        

      
<Button
        // In the click event we are using the initialized StackNavigator and calling the  navigate method.
  onPress={() => this.props.navigation.navigate('HomeScreen' )} 
  title="Back to Home << "
  color="#841584"
  accessibilityLabel="Learn more about this purple button"
/>
      
        </View>
    )
  }
};

const styles = StyleSheet.create({
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
      textAlign: 'center'
  }
});

// You need to export each Component as the default object
export default SecondScreen;