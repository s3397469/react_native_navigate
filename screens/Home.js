// Home Screen

import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

export class Home extends Component {
  render() {
    return (
      <View >

        <Text style={styles.titleText} >
          Home Page{'\n'}{'\n'}
        </Text>
        

        
<Button
        // In the click event we are using the initialized StackNavigator and calling the  navigate method.
         // Send a value to next screen.
  onPress={() => this.props.navigation.navigate('SecondScreen', { customValue: 'This Came  From Home Page!' } )} 
  title="Next Screen >>"
  color="#841584"
  accessibilityLabel="Click to Go to Next page"
/>
      
      </View>

    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
      textAlign: 'center'
  }
});

// You need to export each  Component as the default object
export default Home